#!/usr/bin/env python3
import argparse
import logging
import multiprocessing

import audiomate.corpus.io

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Download data for training.")
    parser.add_argument("target_path", type=str, default=".")
    parser.add_argument("--cv", type=bool, default=False, const=True, nargs="?")
    parser.add_argument(
        "--cv_url",
        type=str,
        default="https://voice-prod-bundler-ee1969a6ce8178826482b88e843c335139bd3fb4.s3.amazonaws.com/"
        "cv-corpus-6.1-2020-12-11/eo.tar.gz",
    )

    args = parser.parse_args()

    logging.basicConfig(level=logging.INFO)

    # monkey patching
    audiomate.corpus.io.common_voice.DOWNLOAD_URLS = {"de": args.cv_url}
    # audiomate.corpus.io.mailabs.DOWNLOAD_URLS = {"de_DE": args.mailabs_url}
    # audiomate.corpus.io.swc.URLS = {"de": args.swc_url}
    from audiomate.corpus.io import (
        TudaDownloader,
        VoxforgeDownloader,
        CommonVoiceDownloader,
        SWCDownloader,
        MailabsDownloader,
    )

    if args.cv:
        dl = CommonVoiceDownloader(lang="de", num_threads=multiprocessing.cpu_count())
        dl.download(args.target_path + "/mozilla")
