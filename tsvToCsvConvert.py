import sys
import csv
import os

# example: python3 tsvToCsvConvert.py myFolder

args = str(sys.argv[1])

arr = os.listdir(f"{args}")
tsvFiles = [x for x in arr if x.endswith(".tsv")]
for file in tsvFiles:   
    tabin = csv.reader(open(f'{args}/{file}'), dialect=csv.excel_tab)
    r = file.replace(".tsv", ".csv")
    commaout = csv.writer(open(f'{args}/{r}', 'w'), dialect=csv.excel)
    print(r)
    for row in tabin:
      commaout.writerow(row)
